

INSERT INTO `artistas` (`id`, `nombre`, `imagen`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Taylor Swift', 'images/tay.png', 'Taylor Swift es una cantante, actriz y compositora estadounidense', '2018-11-01 06:00:00', '2018-11-02 06:00:00'),
(2, 'Eminem', 'images/emi.png', 'Marshall Bruce Mathers III, conocido por su nombre artístico Eminem y también por su álter ego Slim Shady, es un rapero, productor discográfico y actor estadounidense. ', '2018-11-09 06:00:00', '2018-11-08 06:00:00'),
(3, 'Ariana Grande', 'images/ari.png', 'Ariana Grande Butera es una actriz, cantante y filántropa estadounidense.', NULL, NULL),
(4, 'G-Easy', 'images/g.jpg', 'Gerald Earl Gillum, más conocido por su nombre artístico G-Eazy, es un rapero, estadounidense.', NULL, NULL),
(5, 'Nicki Minaj', 'images/nicki.png', 'Nicki Minaj, es una rapera, ​​ cantante, compositora y modelo trinitense.', NULL, NULL),
(6, 'Bruno Mars', 'images/bruno.png', 'Bruno Mars, es un cantante, compositor, productor musical y coreógrafo estadounidense.', NULL, NULL),
(7, 'ZAYN', 'images/zayn.png', ' Zayn, es un cantante y compositor británico, conocido por haber sido miembro de la boy band One Direction.', NULL, NULL),
(8, 'SIA', 'images/sia.png', 'Sia, es una cantante, productora, compositora y actriz australiana. Desde 2018 formó el supergrupo LSD.', NULL, NULL),
(9, 'Cardi B', 'images/cardi.png', 'Cardi B, es una rapera, compositora y actriz estadounidense.', NULL, NULL),
(10, 'Ed Sheeran', 'images/ed.png', 'Ed Sheeran o Ed es un cantante, compositor y guitarrista británico.​', NULL, NULL),
(11, 'Halsey', 'images/halsey.png', 'Halsey es una cantante, diseñadora, pintora, productora y compositora estadounidense', NULL, NULL),
(12, 'Charlie Puth', 'images/charlie.png', 'Charlie Puth es un cantante, compositor y productor musical estadounidense.', NULL, NULL);




INSERT INTO `albums` (`id`, `artista_id`, `nombre`, `anio`, `imagen`,`precio`,  `created_at`, `updated_at`) VALUES
(1, 1, 'Reputation', 2018, 'images/rep.png', 145.90,NULL, NULL),
(2, 2, 'Kamikaze', 2018, 'images/kamikaze.jpg', 235.30,NULL, NULL),
(3, 3, 'sweetener', 2018, 'images/sweetener.jpg', 445.20,NULL, NULL),
(4, 5, 'Queen', 2018, 'images/queen.jpg', 367.85,NULL, NULL),
(5, 6, '24 Magic', 2017, 'images/24.png', 325.92,NULL, NULL),
(6, 4, 'The Beautiful and Damned', 2017, 'images/theb.png', 125.30,NULL, NULL),
(7, 7, 'Mind of Mine', 2016, 'images/mind.jpg', 431.10,NULL, NULL),
(8, 8, 'This is Acting', 2016, 'images/this.png', 99.90,NULL, NULL);



INSERT INTO `cancions` (`id`, `album_id`, `titulo`, `duracion`, `created_at`, `updated_at`) VALUES
(1, 1, 'End Game', '4:05', NULL, NULL),
(2, 1, '...Ready for it?', '3:28', NULL, NULL),
(3, 1, 'I Did Something Bad', '3:58', NULL, NULL),
(4, 1, 'Delicate', '3:52', NULL, NULL),
(5, 1, 'Gorgeous', '3:29', NULL, NULL),
(6, 1, 'Getaway Car', '3:53', NULL, NULL),
(7, 1, 'Call It What You Want', '3:24', NULL, NULL),
(8, 5, '24K Magic', '3:46', NULL, NULL),
(9, 5, 'That\s What I Like It', '3:26', NULL, NULL),
(10, 5, 'Finesse', '3:11', NULL, NULL),
(11, 2, 'Kamikaze', '3:36', NULL, NULL),
(12, 2, 'Normal', '3:42', NULL, NULL),
(13, 2, 'Venom', '4:29', NULL, NULL),
(14, 2, 'Killshot', '4:14', NULL, NULL),
(15, 7, 'Pillowtalk', '3:23', NULL, NULL),
(16, 7, 'Truth', '4:05', NULL, NULL),
(17, 7, 'Drunk', '3:25', NULL, NULL),
(18, 7, 'She', '3:09', NULL, NULL),
(19, 3, 'breathin', '3:18', NULL, NULL),
(20, 3, 'God is a Woman', '3:17', NULL, NULL),
(21, 3, 'No Tears Left To Cry', '3:25', NULL, NULL),
(22, 6, 'Him and I', '4:28', NULL, NULL),
(23, 6, 'Sober', '3:23', NULL, NULL),
(24, 6, 'No Less', '4:10', NULL, NULL),
(25, 8, 'Alive', '4:23', NULL, NULL),
(26, 8, 'Move Your Body', '4:07', NULL, NULL),
(27, 8, 'Unstoppable', '3:37', NULL, NULL);




INSERT INTO `artista_cancion` (`id`, `artista_id`, `cancion_id`, `created_at`, `updated_at`) VALUES
(1, 6, 10, NULL, NULL),
(2, 9, 10, NULL, NULL),
(3, 1, 1, NULL, NULL),
(4, 10, 1, NULL, NULL),
(5, 4, 22, NULL, NULL),
(6, 11, 22, NULL, NULL),
(7, 4, 23, NULL, NULL),
(8, 12, 23, NULL, NULL);



