<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/index');
});


Auth::routes();

Route::get('/index', 'PageController@index')->name('index');
Route::get('/album/{id}', 'PageController@album')->name('album');
Route::get('/albums/', 'PageController@albums')->name('albums');
Route::get('/artista/{id}', 'PageController@artista')->name('artista');
Route::get('/artistas', 'PageController@artistas')->name('artistas');
Route::get('venta/{album_id}', 'PageController@venta')->name('venta');
Route::get('/admin','AdminController@index')->name('dashboard');
Route::get('/admin/clientes','AdminController@clientes')->name('clientes');
Route::get('/admin/ventas','AdminController@ventas')->name('ventas');
Route::get('/admin/tareas','AdminController@tareas')->name('tareas');
Route::get('/admin/agenda','AdminController@agenda')->name('agenda');


Route::post('/ventaFisica', 'VentaController@store')->name('ventaFisica');


