@extends('layouts.app')
@section('content')

<!-- content -->
<div id="content" class="app-content white bg" role="main">

  @include('layouts.parts.nav')
  <div class="app-body" id="view">
    <div class="pos-rlt">
      <div class="page-bg" data-stellar-ratio="2" style="background-image: url('{{ asset($album->imagen)  }}');"></div>
    </div>
    <div class="page-content">
      <div class="padding b-b">
        <div class="row-col">
          <div class="col-sm w w-auto-xs m-b">
            <div class="item w r">
              <div class="item-media">
                <div class="item-media-content" style="background-image: url('{{ asset($album->imagen)  }}');"></div>
              </div>
            </div>
          </div>
          <div class="col-sm">
            <div class="p-l-md no-padding-xs">
              <div class="page-title">
                <h1 class="inline">{{ $album->nombre }} &middot; &nbsp;</h1><h3 class="inline"> {{ $album->anio }}</h3>
                <a href="{{ route('artista', $album->artista->id) }}"><h6>{{ $album->artista->nombre }}</h6></a>
              </div>
             
              <div class="item-action m-b">
                <br>
              <h6>Precio: ${{ $album->precio }}</h6>
              
              <a href="{{ route('venta', $album->id) }}" class="btn circle btn-outline b-primary m-b-lg p-x-md btn-lg">Comprar</a>
                
              </div>
             
            </div>
          </div>
        </div>
      </div>

      <div class="row-col">
        <div class="col-lg-9 b-r no-border-md">
          <div class="padding">

            <h6 class="m-b">
              <span class="text-muted">por</span>
              <a href="{{ route('artista', $album->artista->id) }}" data-pjax class="item-author _600">{{ $album->artista->nombre }}</a>
              <span class="text-muted text-sm">- <?php echo count($canciones); ?> canciones </span>
            </h6>
            <div id="tracks" class="row item-list item-list-xs item-list-li m-b">
              
              
              @foreach($canciones as $cancion)
              <div class="col-xs-12">
                <div class="item r" data-id="item-{{ $cancion->id }}">
                  <div class="item-media ">
                    <a href="#" class="item-media-content" style="background-image: url('images/b6.jpg');"></a>
                  </div>
                  <div class="item-info">
                    <div class="item-title text-ellipsis">
                      <a href="#">{{ $cancion->titulo }}</a>
                    </div>
                   
                    <div class="item-meta text-sm text-muted">
                      <span class="item-meta-right">{{ $cancion->duracion }}</span>
                    </div>


                  </div>
                </div>
              </div>
              @endforeach


            </div>
            <h5 class="m-b">Más de {{ $album->artista->nombre }}</h5>
            <div class="row m-b">
              <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="item r" data-id="item-12" data-src="http://api.soundcloud.com/tracks/174495152/stream?client_id=a10d44d431ad52868f1bce6d36f5234c">
                  <div class="item-media ">
                    <a href="track.detail.html" class="item-media-content" style="background-image: url('images/b11.jpg');"></a>
                    <div class="item-overlay center">
                      <button class="btn-playpause">Play</button>
                    </div>
                  </div>
                  <div class="item-info">
                    <div class="item-overlay bottom text-right">
                      <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                      <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                      <div class="dropdown-menu pull-right black lt"></div>
                    </div>
                    <div class="item-title text-ellipsis">
                      <a href="track.detail.html">Happy ending</a>
                    </div>
                    <div class="item-author text-sm text-ellipsis hide">
                      <a href="artist.detail.html" class="text-muted">Postiljonen</a>
                    </div>
                    <div class="item-meta text-sm text-muted">
                      <span class="item-meta-stats text-xs ">
                        <i class="fa fa-play text-muted"></i> 860
                        <i class="fa fa-heart m-l-sm text-muted"></i> 240
                      </span>
                    </div>


                  </div>
                </div>
              </div>
              
             
              
            </div>
            

          </div>
        </div>
        @include('layouts.parts.footer')


      </div>
    </div>
  </div>

</div>
<!-- / -->


<!-- ############ SWITHCHER START-->
<div id="switcher">
  <div class="switcher white" id="sw-theme">
    <a href="#" data-ui-toggle-class="active" data-ui-target="#sw-theme" class="white sw-btn">
      <i class="fa fa-gear text-muted"></i>
    </a>
    <div class="box-header">
      <strong>Theme Switcher</strong>
    </div>
    <div class="box-divider"></div>
    <div class="box-body">
      <p id="settingLayout" class="hidden-md-down">
        <label class="md-check m-y-xs" data-target="folded">
          <input type="checkbox">
          <i class="green"></i>
          <span>Folded Aside</span>
        </label>
        <label class="m-y-xs pointer" data-ui-fullscreen data-target="fullscreen">
          <span class="fa fa-expand fa-fw m-r-xs"></span>
          <span>Fullscreen Mode</span>
        </label>
      </p>
      <p>Colors:</p>
      <p data-target="color">
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="primary">
          <i class="primary"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="accent">
          <i class="accent"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="warn">
          <i class="warn"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="success">
          <i class="success"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="info">
          <i class="info"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="blue">
          <i class="blue"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="warning">
          <i class="warning"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="danger">
          <i class="danger"></i>
        </label>
      </p>
      <p>Themes:</p>
      <div data-target="bg" class="text-u-c text-center _600 clearfix">
        <label class="p-a col-xs-3 light pointer m-a-0">
          <input type="radio" name="theme" value="" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 grey pointer m-a-0">
          <input type="radio" name="theme" value="grey" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 dark pointer m-a-0">
          <input type="radio" name="theme" value="dark" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 black pointer m-a-0">
          <input type="radio" name="theme" value="black" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
      </div>
    </div>
  </div>
</div>
<!-- ############ SWITHCHER END-->

<!-- ############ SEARCH END -->
@endsection