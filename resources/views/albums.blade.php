@extends('layouts.app')
@section('content')
<div id="content" class="app-content white bg box-shadow-z2" role="main">

@include('layouts.parts.nav')

  
  <div class="app-body" id="view">

    <!-- ############ PAGE START-->

    <div class="page-content">
      <div class="row-col">
        <div class="col-lg-9 b-r no-border-md">
          <div class="padding">


            <div class="page-title m-b">
              <h1 class="inline m-a-0">Albums</h1>
             
            </div>
            <div data-ui-jp="jscroll" class="jscroll-loading-center" data-ui-options="{
            autoTrigger: true,
            loadingHtml: '<i class=\'fa fa-refresh fa-spin text-md text-muted\'></i>',
            padding: 50,
            nextSelector: 'a.jscroll-next:last'
          }">
              <div class="row">


                @foreach($albums as $album)
                <div class="col-xs-4 col-sm-4 col-md-3">
                  <div class="item r" data-id="item-3">
                    <div class="item-media ">
                      <a href="{{ route('album', $album->id) }}" class="item-media-content" style="background-image: url('{{ asset($album->imagen) }}');"></a>
                      
                    </div>
                    <div class="item-info">
                     
                      <div class="item-title text-ellipsis">
                        <a href="{{ route('album', $album->id) }}">{{ $album->nombre }}</a>
                      </div>
                      <div class="item-author text-sm text-ellipsis ">
                        <a href="{{ route('artista', $album->artista->id) }}" class="text-muted">{{ $album->artista->nombre }}</a>
                      </div>
                    </div>
                  </div>
                </div>
                
                @endforeach
                
                
                
               
                
                
             
                
               
              </div>
             
            </div>

          </div>
        </div>


       
        @include('layouts.parts.footer')
      </div>
    </div>

    <!-- ############ PAGE END-->

  </div>
</div>

<div id="switcher">
  <div class="switcher white" id="sw-theme">
    <a href="#" data-ui-toggle-class="active" data-ui-target="#sw-theme" class="white sw-btn">
      <i class="fa fa-gear text-muted"></i>
    </a>
    <div class="box-header">
      <strong>Theme Switcher</strong>
    </div>
    <div class="box-divider"></div>
    <div class="box-body">
      <p id="settingLayout" class="hidden-md-down">
        <label class="md-check m-y-xs" data-target="folded">
          <input type="checkbox">
          <i class="green"></i>
          <span>Folded Aside</span>
        </label>
        <label class="m-y-xs pointer" data-ui-fullscreen data-target="fullscreen">
          <span class="fa fa-expand fa-fw m-r-xs"></span>
          <span>Fullscreen Mode</span>
        </label>
      </p>
      <p>Colors:</p>
      <p data-target="color">
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="primary">
          <i class="primary"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="accent">
          <i class="accent"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="warn">
          <i class="warn"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="success">
          <i class="success"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="info">
          <i class="info"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="blue">
          <i class="blue"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="warning">
          <i class="warning"></i>
        </label>
        <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
          <input type="radio" name="color" value="danger">
          <i class="danger"></i>
        </label>
      </p>
      <p>Themes:</p>
      <div data-target="bg" class="text-u-c text-center _600 clearfix">
        <label class="p-a col-xs-3 light pointer m-a-0">
          <input type="radio" name="theme" value="" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 grey pointer m-a-0">
          <input type="radio" name="theme" value="grey" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 dark pointer m-a-0">
          <input type="radio" name="theme" value="dark" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
        <label class="p-a col-xs-3 black pointer m-a-0">
          <input type="radio" name="theme" value="black" hidden>
          <i class="active-checked fa fa-check"></i>
        </label>
      </div>
    </div>
  </div>
</div>
@endsection()