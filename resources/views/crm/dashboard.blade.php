@extends('crm.layouts.app')
@section('content')

<div class="main-content">
  <!-- Top navbar -->
  @include('crm.layouts.parts.header')
  <!-- Header -->
  <div class="header bg-gradient-danger pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
      <div class="header-body">
        <!-- Card stats -->
        <div class="row">
          <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Tráfico</h5>
                    <span class="h2 font-weight-bold mb-0">2,231</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                    <i class="fas fa-laptop-code"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 1.48%</span>
                  <span class="text-nowrap">Último mes</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Clientes</h5>
                    <span class="h2 font-weight-bold mb-0">356</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                    <i class="fas fa-users"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 0.28%</span>
                  <span class="text-nowrap">Última semana</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Ventas</h5>
                    <span class="h2 font-weight-bold mb-0">124</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                    <i class="fas fa-shopping-basket"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 1.10%</span>
                  <span class="text-nowrap">Último mes</span>
                </p>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Rendimiento</h5>
                    <span class="h2 font-weight-bold mb-0">2,65%</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                      <i class="fas fa-percent"></i>
                    </div>
                  </div>
                </div>
                <p class="mt-3 mb-0 text-muted text-sm">
                  <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                  <span class="text-nowrap">Último mes</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--7">
    <div class="row">
      <div class="col-xl-8 mb-5 mb-xl-0">
        <div class="card bg-gradient-default shadow">
          <div class="card-header bg-transparent">
            <div class="row align-items-center">
              <div class="col">
                <h6 class="text-uppercase text-light ls-1 mb-1">Vision general</h6>
                <h2 class="text-white mb-0">Valor de las ventas</h2>
              </div>
              <div class="col">
                <ul class="nav nav-pills justify-content-end">
                  <li class="nav-item mr-2 mr-md-0">
                    <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                      <span class="d-none d-md-block">Mes</span>
                      <span class="d-md-none">M</span>
                    </a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="card-body">
            <!-- Chart -->
            <div class="chart">
              <!-- Chart wrapper -->
              <canvas id="chart-sales" class="chart-canvas"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4">
        <div class="card shadow">
          <div class="card-header bg-transparent">
            <div class="row align-items-center">
              <div class="col">
                <h6 class="text-uppercase text-muted ls-1 mb-1">Rendimiento</h6>
                <h2 class="mb-0">Total de órdenes</h2>
              </div>
            </div>
          </div>
          <div class="card-body">
            <!-- Chart -->
            <div class="chart">
              <canvas id="chart-orders" class="chart-canvas"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-xl-8 mb-5 mb-xl-0">
        <div class="card shadow">
          <div class="card-header border-0">
            <div class="row align-items-center">
              <div class="col">
                <h3 class="mb-0">URL visitadas</h3>
              </div>
              <div class="col text-right">
                <a href="#!" class="btn btn-sm btn-primary">Ver todo</a>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <!-- Projects table -->
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">URL</th>
                  <th scope="col">Visitantes</th>
                  <th scope="col">Usuarios unicos</th>
                  <th scope="col">Rebote</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">/index</th>
                  <td>4,569</td>
                  <td>340</td>
                  <td><i class="fas fa-arrow-up text-success mr-3"></i> 46,53%</td>
                </tr>
                <tr>
                  <th scope="row">/album</th>
                  <td>3,985</td>
                  <td>319</td>
                  <td><i class="fas fa-arrow-down text-warning mr-3"></i> 46,53%</td>
                </tr>
                <tr>
                  <th scope="row">/artista</th>
                  <td>3,513</td>
                  <td>294</td>
                  <td><i class="fas fa-arrow-down text-warning mr-3"></i> 36,49%</td>
                </tr>
                <tr>
                  <th scope="row">/artistas</th>
                  <td>2,050</td>
                  <td>147</td>
                  <td><i class="fas fa-arrow-up text-success mr-3"></i> 50,87%</td>
                </tr>
                <tr>
                  <th scope="row">/albums</th>
                  <td>1,795</td>
                  <td>190</td>
                  <td><i class="fas fa-arrow-down text-danger mr-3"></i> 46,53%</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-xl-4">
      <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Tareas</h3>
                </div>
                <div class="col text-right">
                  <a href="#" class="btn btn-sm btn-primary">Ver todo</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Vencimiento</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Relizar pago de proovedor #03
                    </td>
                    <td>
                    <span class="badge badge-success">En progreso</span>
                    </td>
                    <td>
                    2018-11-26 18:00:44
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Relizar pago de proovedor #03
                    </td>
                    <td>
                    <span class="badge badge-success">En progreso</span>
                    </td>
                    <td>
                    2018-11-26 18:00:44
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Relizar pago de proovedor #03
                    </td>
                    <td>
                    <span class="badge badge-info">Finalizada</span>
                    </td>
                    <td>
                    2018-11-26 18:00:44
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Relizar pago de proovedor #03
                    </td>
                    <td>
                    <span class="badge badge-danger">Vencida</span>
                    </td>
                    <td>
                    2018-11-26 18:00:44
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Relizar pago de proovedor #03
                    </td>
                    <td>
                    <span class="badge badge-danger">Vencida</span>
                    </td>
                    <td>
                    2018-11-26 18:00:44
                    </td>
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
    <!-- Footer -->
    @include('crm.layouts.parts.footer')
  </div>
</div>
@endsection